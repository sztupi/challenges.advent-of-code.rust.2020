pub fn part1(inp: String) {
    let lines: Vec<i32> = inp
        .split_whitespace()
        .map(|s| s.parse::<i32>().unwrap())
        .collect();

    'outer: for i in 0..lines.len() {
        for j in i..lines.len() {
            if lines[i] + lines[j] == 2020 {
                println!("{} * {} = {}", lines[i], lines[j], lines[i] * lines[j]);
                break 'outer;
            }
        }
    }
}

pub fn part2(inp: String) {
    let lines: Vec<i32> = inp
        .split_whitespace()
        .map(|s| s.parse::<i32>().unwrap())
        .collect();

    'outer: for i in 0..lines.len() {
        for j in i..lines.len() {
            for k in j..lines.len() {
                if lines[i] + lines[j] + lines[k] == 2020 {
                    println!(
                        "{} * {} * {} = {}",
                        lines[i],
                        lines[j],
                        lines[k],
                        lines[i] * lines[j] * lines[k]
                    );
                    break 'outer;
                }
            }
        }
    }
}
